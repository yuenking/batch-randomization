# batch-randomization

For randomization of data when you cannot load everything into memory, and when it's more efficient to load a contiguous block of data. This can be useful when you are loading data from hdf5 format or when you are getting blocks of data from an API.

## How it works

Assuming we know approximately the number of observations in the data, we can then split it into n batches depending on the batch_size desired. We then randomly read x batches into memory, shuffle the data and then return x batches sequentially.

Refer to test_randomizer.py for usage example(s)