import numpy as np


def split_list_into_groups(big_list, batch_size):
    """
    split a list into groups of n with remainder

    >>> split_list_into_groups(range(10), 3)
    [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]

    copied from https://stackoverflow.com/a/1625023
    """
    return [big_list[i:i + batch_size] for i in range(0, len(big_list), batch_size)]


class Randomizer:
    def __init__(self, num_obs, data_getter, data_joiner=None, batch_size=32, num_batches_to_shuffle=10, shuffle=True):
        self.num_obs = num_obs
        self.data_getter = data_getter
        self.batch_size = batch_size
        self.num_batches_to_shuffle = num_batches_to_shuffle
        self.shuffle = shuffle
        if data_joiner:
            self.data_joiner = data_joiner
        else:
            self.data_joiner = np.concatenate

        self.num_batches = int(np.ceil(self.num_obs / self.batch_size))  # TODO ignores leftovers, should be an option
        self.indices = list(range(self.num_batches))
        self.data_generator = None
        self.initialize_generator()

    def initialize_generator(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
        self.data_generator = self.__generator()

    def __generator(self):
        grouped = split_list_into_groups(self.indices, self.num_batches_to_shuffle)
        for batch_refs in grouped:

            # get data from the selected batches and shuffle them
            data = self.data_joiner([self.data_getter(batch_ref) for batch_ref in batch_refs])
            if self.shuffle:
                np.random.shuffle(data)

            # yield the shuffled data batch by batch
            for batch_index in range(len(batch_refs)):
                data_batch = data[batch_index * self.batch_size:(batch_index + 1) * self.batch_size]
                yield data_batch

    def __next__(self):
        return next(self.data_generator)

    def __iter__(self):
        return self
