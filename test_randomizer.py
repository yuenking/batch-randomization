from functools import partial

import numpy as np
import unittest

from randomizer import Randomizer


class MyTestCase(unittest.TestCase):
    def test_without_randomization(self):
        data = np.array([[1, 2], [3, 4], [5, 6]])
        num_obs = data.shape[0]
        batch_size = 1

        def data_getter(d, batch_size, batch_ref):
            return d[batch_ref * batch_size:batch_ref * batch_size + batch_size]

        randomizer = Randomizer(num_obs=num_obs,
                                data_getter=partial(data_getter, data, batch_size),
                                batch_size=batch_size,
                                num_batches_to_shuffle=1,
                                shuffle=False)
        for i, d in enumerate(randomizer):
            np.testing.assert_array_equal(np.array([data[i, :]]), d)


if __name__ == '__main__':
    unittest.main()
